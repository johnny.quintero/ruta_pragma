package com.pragma.ruta_Backend.clienteTest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import com.pragma.ruta_Backend.service.ClienteService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.mockito.BDDMockito.*;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;


@WebMvcTest
public class ClienteControllerTest {

    @InjectMocks
    private MockMvc mockMvc;
    @Mock
    private ClienteService clienteService;
    @InjectMocks
    private ObjectMapper objectMapper;
    /*
    // given - dado o condición previa o configuración
    // when - Accion o comportamiento a probar
    // then - Verificar la salida
    */
    @Test
    void testGuardarCliente() throws Exception {

        // given - dado o condición previa o configuración
        Cliente cliente = new Cliente("Johnny","Quintero Reyes",25,"cedula","6589","Medellin");
        given(this.clienteService.createCliente(any(Cliente.class)))
                .willAnswer((invocation) -> invocation.getArgument(0));

        // when - Accion o comportamiento a probar
        ResultActions response = mockMvc.perform(post("/cliente")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cliente)));

        // then - Verificar la salida
        response.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.nombre",is(cliente.getNombre())))
                .andExpect(jsonPath("$.apellido",is(cliente.getApellido())))
                .andExpect(jsonPath("$.tipoDocumento",is(cliente.getTipoDocumento())))
                .andExpect(jsonPath("$.numeroIdentificacion",is(cliente.getNumeroIdentificacion())));
    }
}
