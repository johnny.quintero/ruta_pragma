package com.pragma.ruta_Backend.clienteTest.repository;

import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import com.pragma.ruta_Backend.repository.cliente.ClienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest //solo pruebas las clases entity, solo la capa de jpa
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

public class ClienteRepositoryTests {

    @Autowired
    private ClienteRepository clienteRepository;

    Cliente cliente;

    @BeforeEach
    void setup(){
        Cliente cliente1 = new Cliente("Johnny","Quintero Reyes",25,"cedula","123","Medellin");
        this.cliente = cliente1;
    }
    @DisplayName("Test para guardar un cliente correctamente")
    @Test
    void testGuardarCliente(){
        // given - dado o condición previa o configuración
        Cliente cliente = new Cliente("Johnny Alexander","Quintero Reyes",25,"cedula","123","Medellin");

        // when - Accion o comportamiento a probar
        Cliente clienteGuardado = this.clienteRepository.save(cliente);

        // then - Verificar la salida
        assertThat(clienteGuardado).isNotNull();
        assertThat(clienteGuardado.getNumeroIdentificacion()).isEqualTo("123");
        assertThat(clienteGuardado.getTipoDocumento()).isEqualTo("cedula");
    }

    @DisplayName("Test para listar los clientes")
    @Test
    void testListarClientes(){

        // given - dado o condición previa o configuración
        Cliente cliente1 = new Cliente("Johnny Alexander","Quintero Reyes",25,"cedula","1234","Medellin");
        this.clienteRepository.save(cliente1);
        this.clienteRepository.save(this.cliente);

        // when - Accion o comportamiento a probar
        List<Cliente> clientes = this.clienteRepository.findAll();
        // then - Verificar la salida
        assertThat(clientes).isNotNull();
        assertThat(clientes.size()).isEqualTo(9);
    }

    @DisplayName("Test obtener cliente por Id")
    @Test
    void testObtenerClientePorId(){
        // given - dado o condición previa o configuración
        this.clienteRepository.save(this.cliente);
        // when - Accion o comportamiento a probar
        Cliente clienteBD = this.clienteRepository.findById(this.cliente.getNumeroIdentificacion()).get();
        // then - Verificar la salida
        assertThat(clienteBD).isNotNull();
    }

    @DisplayName("Test actualizar cliente")
    @Test
    void testActualizarCliente(){
        // given - dado o condición previa o configuración
        this.clienteRepository.save(this.cliente);
        // when - Accion o comportamiento a probar
        Cliente clienteBD = this.clienteRepository.findById(this.cliente.getNumeroIdentificacion()).get();
        clienteBD.setApellido("Pepe lopez");
        clienteBD.setEdad(50);
        clienteBD.setNombre("Alexander");
        Cliente clienteActualizado = this.clienteRepository.save(clienteBD);

        // then - Verificar la salida
        assertThat(clienteActualizado.getApellido()).isEqualTo("Pepe lopez");
        assertThat(clienteActualizado.getEdad()).isEqualTo(50);
        assertThat(clienteActualizado.getNombre()).isEqualTo("Alexander");
    }

    @DisplayName("Test para eliminar un cliente")
    @Test
    void testEliminarClinete(){
        // given - dado o condición previa o configuración
        this.clienteRepository.save(this.cliente);
        // when - Accion o comportamiento a probar
        this.clienteRepository.deleteById(this.cliente.getNumeroIdentificacion());
        Optional<Cliente> clienteOptional = this.clienteRepository.findById(this.cliente.getNumeroIdentificacion());
        // then - Verificar la salida
        assertThat(clienteOptional).isEmpty();
    }
}
