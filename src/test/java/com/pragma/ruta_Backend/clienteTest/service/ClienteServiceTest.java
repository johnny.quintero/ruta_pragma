package com.pragma.ruta_Backend.clienteTest.service;

import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import com.pragma.ruta_Backend.repository.cliente.ClienteRepository;
import com.pragma.ruta_Backend.service.implementation.ClienteImplementation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.*;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ClienteServiceTest {

    @Mock
    private ClienteRepository clienteRepository;

    @InjectMocks
    private ClienteImplementation clienteImplementation;

    Cliente cliente;
    @BeforeEach
    void setup(){
        cliente = new Cliente("Johnny","Quintero Reyes",25,"cedula","6589","Medellin");
    }

    /*
    // given - dado o condición previa o configuración
    // when - Accion o comportamiento a probar
    // then - Verificar la salida
    */
    @DisplayName("Test para guardar cliente")
    @Test
    void testGuardarCliente() {

        // given - dado o condición previa o configuración
        given(this.clienteRepository.findById(this.cliente.getNumeroIdentificacion()))
                .willReturn(Optional.empty());
        given(this.clienteRepository.save(this.cliente))
                .willReturn(this.cliente);

        // when - Accion o comportamiento a probar
        Cliente clienteGuardado = this.clienteImplementation.createCliente(this.cliente);

        // then - Verificar la salida
        assertThat(clienteGuardado).isNotNull();
    }

    @DisplayName("Test para listar clientes")
    @Test
    void testlistarClientes(){
        // given - dado o condición previa o configuración
        Cliente cliente1 = new Cliente("Alexander","Reyes",25,"cedula","1231","Medellin");
        Cliente cliente2 = new Cliente("Sam","prueba",27,"cedula","12314","Medellin");
        given(this.clienteRepository.findAll()).willReturn(List.of(cliente, cliente1,cliente2));

        // when - Accion o comportamiento a probar
        List<Cliente> clientes = this.clienteImplementation.readClientes();

        // then - Verificar la salida
        assertThat(clientes).isNotNull();
        assertThat(clientes.size()).isEqualTo(3);
    }

    @DisplayName("Test para actualizar un cliente")
    @Test
    void testActualizarCliente(){
        // given - dado o condición previa o configuración
        given(this.clienteRepository.save(cliente)).willReturn(cliente);
        cliente.setNombre("Johnny Alexander Quintero");
        cliente.setCiudadNacimiento("Colombia");

        // when - Accion o comportamiento a probar
        Cliente clienteActualizado = this.clienteImplementation.updateCliente(cliente);

        // then - Verificar la salida
        assertThat(clienteActualizado.getNombre()).isEqualTo("Johnny Alexander Quintero");
        assertThat(clienteActualizado.getCiudadNacimiento()).isEqualTo("Colombia");
    }

    @DisplayName("Test para eliminar un cliente")
    @Test
    void testEliminarCliente(){

        // given - dado o condición previa o configuración
        willDoNothing().given(this.clienteRepository).deleteById("6589");;

        // when - Accion o comportamiento a probar
        this.clienteImplementation.deleteCliente("6589");

        // then - Verificar la salida
        verify(this.clienteRepository, times(1)).deleteById("6589");
    }

}
