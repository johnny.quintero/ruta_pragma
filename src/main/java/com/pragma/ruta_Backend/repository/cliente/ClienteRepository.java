package com.pragma.ruta_Backend.repository.cliente;

import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,String> {
}
