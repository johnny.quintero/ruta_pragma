package com.pragma.ruta_Backend.repository.imagen;

import com.pragma.ruta_Backend.Resource.imagen.ImagenMongo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImagenRepository extends MongoRepository<ImagenMongo,String> {
}
