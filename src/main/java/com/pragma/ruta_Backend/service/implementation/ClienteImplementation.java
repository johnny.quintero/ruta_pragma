package com.pragma.ruta_Backend.service.implementation;

import com.pragma.ruta_Backend.repository.cliente.ClienteRepository;
import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import com.pragma.ruta_Backend.service.ClienteService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteImplementation implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;
    @Override
    public Cliente createCliente(Cliente cliente) {
        Optional<Cliente> buscarCliente = this.findByCliente(cliente.getTipoDocumento(), cliente.getNumeroIdentificacion());
        if(buscarCliente.isPresent()){
            return null;
        }
        Cliente client = this.clienteRepository.save(cliente);
        return client;
    }

    @Override
    public List<Cliente> readClientes() {
        return this.clienteRepository.findAll();
    }

    @Override
    public Cliente updateCliente(Cliente cliente) {
        Cliente client = this.clienteRepository.save(cliente);
        return client;
    }

    @Override
    public void deleteCliente(String idCliente) {
        this.clienteRepository.deleteById(idCliente);
    }

    @Override
    public Optional<Cliente> findByCliente(String tipoDocumento, String numeroIdentificacion) {
        return this.clienteRepository.findAll().stream()
                .filter(data -> tipoDocumento.equals(data.getTipoDocumento()) && numeroIdentificacion.equals(data.getNumeroIdentificacion()))
                .findFirst();
    }


}
