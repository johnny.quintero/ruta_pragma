package com.pragma.ruta_Backend.service.implementation;

import com.pragma.ruta_Backend.Resource.imagen.ImagenMongo;
import com.pragma.ruta_Backend.repository.imagen.ImagenRepository;
import com.pragma.ruta_Backend.service.ImagenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ImagenImplementation implements ImagenService {


    @Autowired
    ImagenRepository imagenRepository;

    @Override
    public ImagenMongo createImagen(ImagenMongo imagen) {
        ImagenMongo image = this.imagenRepository.save(imagen);
        return image;
    }

    @Override
    public List<ImagenMongo> readImages() {
        return this.imagenRepository.findAll();
    }

    @Override
    public ImagenMongo updateImagen(ImagenMongo imagen) {
        ImagenMongo image = this.imagenRepository.save(imagen);
        return image;
    }

    @Override
    public void deleteImagen(String idCliente) {
        String consulta = this.imagenRepository.findAll().stream()
                .filter((data) -> data.getIdCliente().equals(idCliente))
                .findFirst()
                .get()
                .getId();
        System.out.println(consulta);
        this.imagenRepository.deleteById(consulta);
    }

    @Override
    public Optional<ImagenMongo> findByCliente(String numeroIdentificacion, String tipoDocumento) {
        return this.imagenRepository.findAll().stream()
                .filter((data) -> data.getIdCliente().equals(numeroIdentificacion) && data.getTipoDocumento().equals(tipoDocumento))
                .findFirst();
    }
}
