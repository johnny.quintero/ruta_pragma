package com.pragma.ruta_Backend.service;

import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ClienteService {
    @Transactional
    public Cliente createCliente(Cliente cliente) throws Exception;
    @Transactional(readOnly = true)
    public List<Cliente> readClientes();
    @Transactional
    public Cliente updateCliente(Cliente cliente);
    @Transactional
    public void deleteCliente(String numeroIdentificacion);
    @Transactional(readOnly = true)
    public Optional<Cliente> findByCliente(String tipoDocumento, String numeroIdentificacion);
}
