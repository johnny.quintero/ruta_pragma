package com.pragma.ruta_Backend.service;


import com.pragma.ruta_Backend.Resource.imagen.ImagenMongo;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ImagenService {
    @Transactional
    public ImagenMongo createImagen(ImagenMongo imagen);
    @Transactional(readOnly = true)
    public List<ImagenMongo> readImages();
    @Transactional
    public ImagenMongo updateImagen(ImagenMongo imagen);
    @Transactional
    public void deleteImagen(String idCliente);
    @Transactional(readOnly = true)
    public Optional<ImagenMongo> findByCliente(String numeroIdentificacion, String tipoDocumento);
}
