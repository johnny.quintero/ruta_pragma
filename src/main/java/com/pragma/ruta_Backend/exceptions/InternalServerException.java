package com.pragma.ruta_Backend.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class InternalServerException extends RuntimeException{
    private String code;
    private HttpStatus status;
    public InternalServerException(String code, HttpStatus status, String message){
        super(message);
        this.code = code;
        this.status = status;
    }
}
