package com.pragma.ruta_Backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class RutaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RutaBackendApplication.class, args);
	}

}
