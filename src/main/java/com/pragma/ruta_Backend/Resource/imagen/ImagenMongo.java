package com.pragma.ruta_Backend.Resource.imagen;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotBlank;

@Document(collection = "imagen")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImagenMongo {

    @Id
    private String id;

    @Field("idCliente")
    @NotBlank
    private String idCliente;

    @Field("urlImagen")
    @NotBlank
    private String urlImagen;

    @Field("tipoDocumento")
    @NotBlank
    private String tipoDocumento;
}
