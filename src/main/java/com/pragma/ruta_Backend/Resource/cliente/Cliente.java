/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pragma.ruta_Backend.Resource.cliente;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author johnny.quintero_prag
 */
@Entity
@Data
@Table(name = "cliente")
@AllArgsConstructor
@NoArgsConstructor
public class Cliente  {

    @Column(name = "nombre")
    @NotBlank
    private String nombre;
    @Column(name = "apellido")
    @NotBlank
    private String apellido;
    @Column(name = "edad")
    @NotNull
    private Integer edad;
    @Column(name = "tipo_documento")
    @NotBlank
    private String tipoDocumento;
    @Id
    @Basic(optional = false)
    @Column(name = "numero_identificacion")
    @NotBlank
    private String numeroIdentificacion;
    @Column(name = "ciudad_nacimiento")
    @NotBlank
    private String ciudadNacimiento;
}
