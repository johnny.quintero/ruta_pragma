package com.pragma.ruta_Backend.controller;

import com.pragma.ruta_Backend.exceptions.InternalServerException;
import com.pragma.ruta_Backend.exceptions.MessageError;
import com.pragma.ruta_Backend.exceptions.RequestException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler(value = RequestException.class)
    public ResponseEntity<MessageError> requestExceptionHandler(RequestException ex){
        MessageError messageError = MessageError.builder().code(ex.getCode()).message(ex.getMessage()).build();
        return new ResponseEntity<>(messageError, ex.getStatus());
    }

    @ExceptionHandler(value = InternalServerException.class)
    public ResponseEntity<MessageError> internalServerExceptionHandler(InternalServerException ex){
        MessageError messageError = MessageError.builder().code(ex.getCode()).message(ex.getMessage()).build();
        return new ResponseEntity<>(messageError, ex.getStatus());
    }
}
