package com.pragma.ruta_Backend.controller;

import com.pragma.ruta_Backend.Resource.imagen.ImagenMongo;
import com.pragma.ruta_Backend.exceptions.InternalServerException;
import com.pragma.ruta_Backend.exceptions.RequestException;
import com.pragma.ruta_Backend.service.implementation.ImagenImplementation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/imagen")
@CrossOrigin(origins = "*")
@Slf4j
public class ImagenController {

    @Autowired
    ImagenImplementation imagenImplementation;

    @PostMapping()
    public ResponseEntity<?> createImagen(@Valid @RequestBody ImagenMongo imagen, BindingResult br){
        if (br.hasErrors()) {
            throw new RequestException("400", HttpStatus.BAD_REQUEST, br.getAllErrors().toString());
        }
        ImagenMongo image = this.imagenImplementation.createImagen(imagen);
        return new ResponseEntity(image,HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> readImages(){
        List<ImagenMongo> imagenes = this.imagenImplementation.readImages();
        if(imagenes.isEmpty()){
            throw new InternalServerException("500", HttpStatus.INTERNAL_SERVER_ERROR, "No hay datos");
        }
        return new ResponseEntity(imagenes,HttpStatus.OK);
    }

    @DeleteMapping("/{idCliente}")
    public ResponseEntity<?> deleteImagen(@PathVariable String idCliente){
        try{
            this.imagenImplementation.deleteImagen(idCliente);
            return new ResponseEntity("Imagen eliminada",HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<ObjectError>(new ObjectError("imagen","hubo un error al eliminar la imagen"), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    @PutMapping()
    public ResponseEntity<?> updateImage(@Valid @RequestBody ImagenMongo imagen, BindingResult br){
        if (br.hasErrors()) {
            throw new RequestException("400",HttpStatus.BAD_REQUEST, br.getAllErrors().toString());
        }
        Optional<ImagenMongo> image = this.imagenImplementation.findByCliente(imagen.getIdCliente(), imagen.getTipoDocumento());
        if(image.isEmpty()){
            throw new RequestException("400",HttpStatus.BAD_REQUEST,"Usuario No se encuentra registrado");
        }
        ImagenMongo imageActualizada = image.get();
        imageActualizada.setUrlImagen(imagen.getUrlImagen());
        ImagenMongo img = this.imagenImplementation.updateImagen(imageActualizada);
        return new ResponseEntity(img,HttpStatus.OK);
    }
}
