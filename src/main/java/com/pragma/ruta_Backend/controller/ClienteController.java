package com.pragma.ruta_Backend.controller;

import com.pragma.ruta_Backend.exceptions.InternalServerException;
import com.pragma.ruta_Backend.exceptions.RequestException;
import com.pragma.ruta_Backend.Resource.cliente.Cliente;
import com.pragma.ruta_Backend.service.implementation.ClienteImplementation;
import org.bson.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cliente")
@CrossOrigin(origins = "*")

public class ClienteController {
    @Autowired
    ClienteImplementation clienteImplementation;

    @PostMapping("")
    public ResponseEntity<?> createCliente(@RequestBody @Valid Cliente cliente, BindingResult br) {
        if (br.hasErrors()) {
            throw new RequestException("400",HttpStatus.BAD_REQUEST, br.getAllErrors().toString());
        }
        Cliente clienteGuardado = this.clienteImplementation.createCliente(cliente);
        if(clienteGuardado == null){
            throw new RequestException("400",HttpStatus.BAD_REQUEST,"Usuario ya registrado con el mismo tipo de documento y número de identificacion");
        }
        return new ResponseEntity(clienteGuardado,HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<?> readClientes(){
        List<Cliente> clientes = this.clienteImplementation.readClientes();
        if(clientes.isEmpty()){
            throw new InternalServerException("500", HttpStatus.INTERNAL_SERVER_ERROR, "No hay datos");
        }
        return new ResponseEntity(clientes, HttpStatus.OK);
    }

    @DeleteMapping("/{numeroIdentificacion}")
    public ResponseEntity<?> deleteCliente(@PathVariable String numeroIdentificacion){
        try{
            this.clienteImplementation.deleteCliente(numeroIdentificacion);
            return new ResponseEntity("cliente eliminado",HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<ObjectError>(new ObjectError("cliente","hubo un error al eliminar el cliente"), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PutMapping("")
    public ResponseEntity<?> updateCliente(@Valid @RequestBody Cliente cliente, BindingResult br){
        if (br.hasErrors()) {
            throw new RequestException("400",HttpStatus.BAD_REQUEST, br.getAllErrors().toString());
        }
        Optional<Cliente> buscarCliente = this.clienteImplementation.findByCliente(cliente.getTipoDocumento(), cliente.getNumeroIdentificacion());
        if(buscarCliente.isEmpty()){
            throw new RequestException("400",HttpStatus.BAD_REQUEST,"Usuario No se encuentra registrado");
        }
        this.clienteImplementation.updateCliente(cliente);
        return new ResponseEntity(cliente,HttpStatus.ACCEPTED);
    }
}
